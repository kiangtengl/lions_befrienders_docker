FROM elixir:latest
MAINTAINER kiangtengl

# Run commands in bash
RUN ln -snf /bin/bash /bin/sh

ENV REFRESHED_AT 2017-10-02
ENV ELIXIR_VERSION 1.5.2
ENV HOME /root

# Add local node module binaries to PATH
ENV PATH $PATH:node_modules/.bin:/opt/elixir-${ELIXIR_VERSION}/bin

# Install Hex+Rebar
RUN mix local.hex --force && \
    mix local.rebar --force && \
    mix hex.info

# Install Latest Version of Phoenix
RUN mix archive.install https://github.com/phoenixframework/archives/raw/master/phx_new.ez

# Install Packages
RUN apt-get update && apt-get install -y \
    apt-transport-https \
    postgresql-client \
    inotify-tools \
    erlang-tools \
    build-essential

# Install NVM
ENV NVM_DIR /usr/local/nvm
ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.5/install.sh | bash -
RUN source $NVM_DIR/nvm.sh \
    && nvm install node \
    && nvm alias default node \
    && nvm use default

# Install Yarn and Node
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install -y yarn