# Lions Befriender Docker Image

A docker image with Phoenix v1.3 for use in lions_befrienders

# Useful Docker Commmands

## Docker
```
# List Images
docker ps

# Build Dockerfile (in current directory)
docker build -t some_tag_name .

# Start Image as a Container
docker run s-i -t ome_tag_name /bin/bash

# Remove all Images
docker rmi $(docker images -f dangling=true -q)

# Delete all containers
docker rm $(docker ps -aq)
```